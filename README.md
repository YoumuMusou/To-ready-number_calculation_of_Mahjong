To-ready-number Calculation of Mahjong

---

## Project Introduction

This code is to calculate the to-ready-number of a mahjong hand. A ready hand's to-ready-number is 0, like 1112345678999m. A win hand's to-ready-number is -1, like 11223344556677z. This version supports

> * Japanese Mahjong
> * Chinese National Standard (GB Mahjong)
> * Zung Jung Mahjong

And any combination of the kinds of win hands of them.

---
### Development Envrionment SetUp

* Development Software

    Please install the following software before. 

> * Python (2.7 preferred, not tested on 3 yet)

* Usage:

```
./syanten.py -p $PAI -m $MODE
```
* $PAI is the tiles on hand, like 1112345678999m (default value).
* $MODE is bit expression of combination of win hands and style of seven pairs.
* 1 for regular hand, 2 for seven pairs, 4 for thirteen orphans, 8 for knitted, 16 for knitted+regular, 32 for Japanese mode of seven pairs.
* E.g., Japanese all hands is 39, Zung Jung all hands is 7, Chinese National Standard all hands is 31, regular hands only is 1.
* Only support 3*n+1, 3*n+2 tiles with n <= 4. When n < 3, only calculate regular hand.

* Reference:

> * http://mahjong.org
> * http://tenhou.net
